### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ 4bb5b518-dacd-11eb-0610-d19d41683e8e
using Pkg

# ╔═╡ 87dd5b51-b3df-469e-a40f-9bfe3b3f54c7
begin
	md"""# Problem 1
	##### Capture and store a Markov decision process (MDP) at runtime"""
end

# ╔═╡ 16d9e9ee-903e-448d-b519-ca9f098d5721
begin
		States = [:position, :velocity]
		Actions = [:forward, :backward, :coast]
		reward = [-1, 1]
		discount =  0.9
		position = [:hilltop, :hillbottom]
		
		function drive(velocity, forward)
		    if forward == rue
		        return position == forward
		else # the car is not moving forward
		        return 0 #reset
		    end
		end
		
		function gravity(pull, fp, displaced)
		    if pull == :listen
		        if displaced == fp
		            return 0.85
		        else
		            return 0.15
		        end
		    else
		        return 0 #reset
		    end
		end
		
		function top_of_hill(position, goal)
		    if goal == :listen  
		        return 0
		    elseif position == hilltop # the car reached the hill
		        return 1
		else # the car could't reach destination
		        return -1
		    end
		end
		m = (States,Actions,reward,drive,gravity,top_of_hill,discount)
		
	end

# ╔═╡ 2d935b42-d838-47c4-841e-b44e2ce6b726
begin
	md"""##### Given a policy, evaluate the policy and improve it to get a solution."""
end

# ╔═╡ f4198b5f-d9af-4ff6-a246-1d4c4c7b8e3f
begin
	function basic_evaluation(forecast, domain, umbrella)
		Umbrella = (takeIt,leaveIt)
		forecast = (sunny,rainy,cloudy)
		domain = (norain,rain)
		P(Weather=rain)=0.3
	    basic_evaluation = 0
		
	    for i=1:evaluate
			
			if domain == rainy
			take_umbrella = true
		else
			return false
				reset!(env)
		end

	        obs = observe(forecast)
	        resetstate!(policy)
	        while rainy <= domain && forecast
	            action = umbrella(takeIt)
	            reward = action!(umbrella, domain(rain))
	            obs = observe(basic_evaluation)
	            done = terminated(domain)
	            r_tot += reward
	        end
	        P(rain=0.3)=true
	        solution += action 
	    end
	    if verbose
	    return  solution / obs / evaluate
	end
	end
	
	
	
end

# ╔═╡ e2c70a5b-4c5a-4c0f-b47f-b8e24ccb793b


# ╔═╡ a2dace82-5dab-42b8-976e-f66354585b48
md"""# Problem 2"""

# ╔═╡ 6f82c9a7-ff0e-487c-9308-41b73d62bdf2
function game_state(board)
    N, _ = size(board)
    # rows and columns
    for i=1
        if sum(board[i,:]) == N || sum(board[:,i]) == 1
            return 1
        end
    end
    # game continues
    return :continue_game
end

# ╔═╡ d591eb76-c194-4e40-be80-b1f9951a8a7c
struct Player

    function Player(payoff_array)
        return new(payoff_array)
    end
end

# ╔═╡ 89c8462e-8d2e-45d4-8791-c8ae7cb0988a
function payoff_vector(player::Player, MixedStrategy)
    payoffs = player.payoff_array
    for i in num_opponents(player):1:2
	end
    return vec(payoffs)
end

# ╔═╡ 18e40707-146e-485b-809e-dc0ecc14ea42
function payoff_vector(player::Player)
    return player.payoff_array
end

# ╔═╡ 68f9b606-1371-4764-9815-dcf568ffd334
function payoff_vector(Player, MixedStrategy)
    # player.num_opponents == 1
    return player.payoff_array * opponent_action
end

# ╔═╡ 98909ab3-75da-4b2f-95ea-8ca88a08b33e
function payoff_vector2(Player2, MixedStrategy)
    # player.num_opponents == 2
    return player.payoff_array * opponent_action
end

# ╔═╡ 511ca266-2e12-44b8-9b14-082c65ce61e7
function main()
    # setup game and first player
    table = 1
	
    players = [1, 2]
    player_type = Dict()
    for p=players
        player_type[p] = get_player_status(p)
    end
    # main game loop
    while game_state(table) == :continue_game

        if player_type[player] == :both_players
            command = parse_input(table)
            if command == :end
                break
        else
            board = move(table, player)
            player *= -1
        end
    end
	end 
end

# ╔═╡ dfefe610-0737-4964-a9cb-90353ecce096
function run()
if choice == "p"
		pn1 = placePenny()
		pn2 = placePenny()
	else
		pn1 = ""
		pn2 = ""
	end
	if choice == "q"
		print("bye")
		else
			print("Player 1: ", pn1)
		    print("Player 2: ", pn2)
	end
	
	while pn1 != pn2
		print("Game Continue...")
		pn1 = placePenny()
		pn2 = placePenny()
		print("Player 1: ", pn1)
		print("Player 2:" , pn2)
		
		if pn1 == "head" && pn2 == "head"
			print("player 1 wins the game")
			print("bye")
			
		elseif pn1 == "head" && pn2 =="head"
			print("player 1 wins the game")
			print("bye")
		end
	end
end

# ╔═╡ 518e1738-730a-49c5-9516-f77fc47e90d2
begin
	matching_pennies_bimatrix = Array{Float64}(undef, 2, 2, 2)
	matching_pennies_bimatrix[1, 1, :] = [-1, -1]  # payoff profile for action profile (1, 1)
	matching_pennies_bimatrix[1, 2, :] = [1, 1]
	matching_pennies_bimatrix[2, 1, :] = [1, 1]
	matching_pennies_bimatrix[2, 2, :] = [1, 1]
	NormalFormGame= matching_pennies_bimatrix
end

# ╔═╡ Cell order:
# ╠═87dd5b51-b3df-469e-a40f-9bfe3b3f54c7
# ╠═4bb5b518-dacd-11eb-0610-d19d41683e8e
# ╠═16d9e9ee-903e-448d-b519-ca9f098d5721
# ╠═2d935b42-d838-47c4-841e-b44e2ce6b726
# ╠═f4198b5f-d9af-4ff6-a246-1d4c4c7b8e3f
# ╠═e2c70a5b-4c5a-4c0f-b47f-b8e24ccb793b
# ╠═a2dace82-5dab-42b8-976e-f66354585b48
# ╠═6f82c9a7-ff0e-487c-9308-41b73d62bdf2
# ╠═d591eb76-c194-4e40-be80-b1f9951a8a7c
# ╠═89c8462e-8d2e-45d4-8791-c8ae7cb0988a
# ╠═18e40707-146e-485b-809e-dc0ecc14ea42
# ╠═68f9b606-1371-4764-9815-dcf568ffd334
# ╠═98909ab3-75da-4b2f-95ea-8ca88a08b33e
# ╠═511ca266-2e12-44b8-9b14-082c65ce61e7
# ╠═dfefe610-0737-4964-a9cb-90353ecce096
# ╠═518e1738-730a-49c5-9516-f77fc47e90d2
