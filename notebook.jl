### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ c0082a1e-0c50-4e10-96d2-e5d703fac011
begin
	using Pkg
	include("Assignment.jl")
end

# ╔═╡ 11b1a8c4-edb6-499c-85a3-15e62405226d
function NormalFormGame(players)
    is_consistent(players) ||
        throw(ArgumentError)
    return NormalFormGame(players)
end

# ╔═╡ 972fb0a0-8156-46c7-bd23-29b36cff3c8b
function table(new)
    # creates an empty table
    row = [Char(1)]
    columns = [1:N]
end


# ╔═╡ 92581ae0-7219-4ade-b050-159d02223792
function winner(table, payoff)
    b = copy(board)
    i, j = payoff
    if b[i,j]
        :b
end
end

# ╔═╡ 251d30ed-6b2b-4a68-89d3-b3eb35567107
function penny_side(x)
    if side == -1
        loser = "null"
    elseif side == 1
        winner = "+1"
	end
end


# ╔═╡ 2a2a9e96-2d44-4555-847c-15464aee3617
function winner_status(player)
    winner_string = (winner == 1)
    if winner == player
        :calculate_payoff
		reset!
        get_winner_status(player)
    end
end

# ╔═╡ fb7d5fab-9bf3-432d-b44c-c96f137680c1
function payoff(winner)
	if winner
		return 1
		give_coin
		payoff
	end
end

# ╔═╡ Cell order:
# ╠═c0082a1e-0c50-4e10-96d2-e5d703fac011
# ╠═11b1a8c4-edb6-499c-85a3-15e62405226d
# ╠═972fb0a0-8156-46c7-bd23-29b36cff3c8b
# ╠═92581ae0-7219-4ade-b050-159d02223792
# ╠═251d30ed-6b2b-4a68-89d3-b3eb35567107
# ╠═2a2a9e96-2d44-4555-847c-15464aee3617
# ╠═fb7d5fab-9bf3-432d-b44c-c96f137680c1
